# Ionic 2 Template with some additional interfaces build by me #

Basic skeleton based on ng2-translate, indexedDb. Introduces simple mommy app for completing baby memory milestones by capturing photos locally or on Facebook.
From the cordova side we have ready commands for camera, files, files transfer, facebook graph, auth and rest call for picture upload. 

* Version 1.0

### Structure ###

* The main domains are located under src/app/domains.
* The interfaces are located under src/app/infrastructure.
* The ui components are located under src/pages.

### How to run it? ###

You should be able to run it by following the steps from here:
https://github.com/driftyco/ionic-conference-app

* Clone this repository.
* Run npm install from the project root.
* Install the ionic CLI (npm install -g ionic)
* Run ionic serve in a terminal from the project root.

Note: Is your build slow? Update npm to 3.x: npm install -g npm.

### Running on Android ###

You can download the android SDK and the Android emulator and run it like every cordova app.

### TODO ###
* Unit tests
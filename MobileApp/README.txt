You should be able to run it by following the steps from here:
https://github.com/driftyco/ionic-conference-app

Clone this repository.
Run npm install from the project root.
Install the ionic CLI (npm install -g ionic)
Run ionic serve in a terminal from the project root.
import { Component, ViewChild } from '@angular/core';
import { Platform } from 'ionic-angular';
import { StatusBar } from 'ionic-native';
import { App, MenuController, Nav } from 'ionic-angular';
import { TranslateService } from 'ng2-translate/ng2-translate';

// Pages.
// import { TabsPage } from './pages/tabs/tabs';
import { HomePage } from '../pages/home/home';
import { AchievementsPage } from '../pages/achievements/achievements';
import { AlbumPage } from '../pages/albums/album';
import { CategoriesPage } from '../pages/categories/categories';
import { ProfilesPage } from '../pages/profiles/profiles';
import { SettingsPage } from '../pages/settings/settings';
import { SkinsPage } from '../pages/skins/skins';

// Query.
import { QueryBaseDataProvider } from './queries/query.base.data.provider';
import { QueryGenericDataProvider } from './queries/query.generic.data.provider';
import { QueryIndexedDbDataStrategy } from './queries/query.indexed.db.data.strategy';
import { QueryHttpDataStrategy } from './queries/query.http.data.strategy';
import { GenericQuery } from './queries/generic.query';

// Command.
import { CommandBaseDataProvider } from './commands/command.base.data.provider';
import { CommandGenericDataProvider } from './commands/command.generic.data.provider';
import { CommandIndexedDbDataStrategy } from './commands/command.indexed.db.data.strategy';
import { CommandHttpDataStrategy } from './commands/command.http.data.strategy';
import { PutCommand } from './commands/put.command';
import { PostCommand } from './commands/post.command';

// Events.
import { ProfileEvent } from './events/profile.event';

// Other.
import { DataList } from './infrastructure/data.list';
import { Context } from './infrastructure/context';
import { IndexedDbDataSeeds } from './commands/indexed.db.data.seeds';
import { AdMobProvider } from './infrastructure/ad.mob.provider';
import { LoggerProvider } from './infrastructure/logger.provider';
import { LoggerLevel } from './infrastructure/logger';

// Test.
import { GenericComponent } from '../pages/home/generic.component';

// Domains.
import { CategoriesMode } from './domains/category';


@Component({
    templateUrl: `./app.component.html`
})
export class MyApp {
    private rootPage = HomePage;
    private pages: Array<{ title: string, component: any, icon: string, params?: any }>;
    private profile: any;
    @ViewChild(Nav) nav: Nav;

    constructor(
        platform: Platform,
        app: App,
        private context: Context,
        private seeds: IndexedDbDataSeeds,
        private adMobProvider: AdMobProvider,
        private translate: TranslateService,
        private logger: LoggerProvider,
        private profileEvent: ProfileEvent
    ) {
        // Logging setup.
        logger.setLevel(LoggerLevel.Info);
        logger.enableConsole();

        // used for an example of ngFor and navigation
        this.pages = [
            { title: 'Home', component: HomePage, icon: "home" },
            { title: 'Categories', component: CategoriesPage, icon: "camera" },
            { title: 'Albums', component: CategoriesPage, icon: "images", params: { categoriesMode: CategoriesMode.Album } },
            { title: 'Achievements', component: AchievementsPage, icon: "trophy" },
            { title: 'Profiles', component: ProfilesPage, icon: "person" },
            { title: 'Settings', component: SettingsPage, icon: "settings" }
        ];

        platform.ready().then(() => {
            StatusBar.styleDefault();
        });

        seeds.seed(true); // For testing only

        if (context.isPaid() === false) {
            this.adMobProvider.createBanner();
            this.adMobProvider.prepareInterstitial();
        }

        
        translate.setDefaultLang('en');
        translate.use('en');

        // Load current profile
        this.profile = context.getActiveProfile();

        // Load current profile if changed.
        this.profileEvent.onUpdated.subscribe(result => {
            this.profile = context.getActiveProfile();
        });

        logger.info(`MyApp has started`);
    }

    openPage(page) {

        if (page.params !== undefined) {
            this.nav.setRoot(page.component, page.params);
        } else {
            this.nav.setRoot(page.component);
        }
    }
}


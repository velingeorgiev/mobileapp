﻿import { DateProvider } from '../infrastructure/date.provider';

export class Profile {
    public id: number;
    public name: string;
    public created: string;
    public modified: string;
    public isActive: boolean;
    public age: number;

    static create(name: string): Profile {
        let profile = new Profile();
        let now = DateProvider.now();
        profile.name = name;
        profile.created = now;
        profile.modified = now;
        profile.isActive = true;

        return profile;
    }
}
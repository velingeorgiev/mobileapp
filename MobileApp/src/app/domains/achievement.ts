﻿import { DateProvider } from '../infrastructure/date.provider';

export class Achievement {
    public id: number;
    public name: string;
    public count: number;
    public picture: string;
    public target: number;
    public description: string;
    public completed: boolean;
    public modified: string;

    static create(jsonObject: any): Achievement {

        let achievement = new Achievement();
        achievement.id = jsonObject.id;
        achievement.name = jsonObject.name;
        achievement.count = jsonObject.count;
        achievement.target = jsonObject.target;
        achievement.modified = jsonObject.modified;
        achievement.completed = (jsonObject.count == jsonObject.target);

        if (achievement.count === achievement.target) {
            achievement.picture = `assets/achievements/${achievement.name}_color.svg`;
        } else {
            achievement.picture = `assets/achievements/${achievement.name}.svg`;
        }

        achievement.description = `desc_${achievement.name}`; // To be used alosng with TranslateService in the templates.

        return achievement;
    }

    increment(): void {
        this.count += 1;
        this.updateCompleted();
    }

    private updateCompleted(): void {
        this.completed = (this.count == this.target);
    }
}
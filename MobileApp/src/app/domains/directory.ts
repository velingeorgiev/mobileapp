﻿import { Observable } from 'rxjs/Observable';

declare let cordova: any;

export class Directory {
    public name: string;
    public fullPath: string;
    public parentDir: string;

    static create(name: string): Directory {
        let appDir = cordova.file.externalDataDirectory; // Importnant: Has ending slash "/"; TODO: cange for iOS. 
        let dir = new Directory();
        dir.name = name;
        dir.fullPath = `${appDir}${name}`;
        dir.parentDir = appDir;
        return dir;
    }
}
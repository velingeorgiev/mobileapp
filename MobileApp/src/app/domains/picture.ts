﻿import { Directory } from './directory';
import { DateProvider } from '../infrastructure/date.provider';

export class Picture {
    public name: string;
    public fullPath: string;
    public thumbFullPath: string;
    public parentDirName: string;
    public parentDirPath: string;

    static create(fullPath: string, thumbFullPath?: string): Picture {
        let picture = new Picture();

        picture.fullPath = fullPath;
        picture.thumbFullPath = thumbFullPath;

        let path = picture.fullPath;
        let lastIndex = path.lastIndexOf("/");

        picture.name = path.substring(lastIndex + 1, path.length);
        picture.parentDirPath = path.substring(0, lastIndex + 1);

        path = path.substring(0, lastIndex); // Remove last slash.
        lastIndex = path.lastIndexOf("/");
        picture.parentDirName = path.substring(lastIndex + 1, path.length);

        return picture;
    }
}

export class PictureMoveInfo {

    public profileDir: Directory;
    public picture: Picture;

    static create(pictureFullPath: string, profileDirName: string): PictureMoveInfo {
        let info = new PictureMoveInfo();
        info.picture = Picture.create(pictureFullPath);
        info.profileDir = Directory.create(profileDirName);
        return info;
    }
}
﻿import { DateProvider } from '../infrastructure/date.provider';

export class Category {
    public id: number;
    public name: string;
    public type: CategoryType;
    public active: boolean;
    public total: number;
    public completed: number;
}

export enum CategoryType {
    Unlimited = 0,
    Limited = 1
}

export enum CategoriesMode {
    Normal = 0, // Why? Coz I have issue with the typescript compilation. Also this this.nav.push(CategoriesPage, { mode: CategoriesMode.HideCategories }); yelded "undefined".
    Album = 1,
    HideCategories = 2,
    HideChallenges = 3
}
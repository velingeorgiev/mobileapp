﻿import { Observable } from 'rxjs/Observable';
import { Picture } from '../domains/picture';
import { Profile } from '../domains/profile';
import { DateProvider } from '../infrastructure/date.provider';

export class Challenge {
    public id: number;
    public name: string;
    public pictureUrl: string;
    public thumbUrl: string;
    public memory: string;
    public profileId: number;
    public categoryId: number;
    public facebookId: string;
    public facebookPostId: string;
    public created: string;

    static create(name: string, categoryId: number): Challenge {
        let challenge = new Challenge();
        challenge.name = name;
        challenge.categoryId = categoryId;
        challenge.created = DateProvider.now();
        return challenge;
    }

    setPicture(picture: Picture): Challenge {
        this.pictureUrl = picture.fullPath;
        this.thumbUrl = picture.thumbFullPath;
        return this;
    }

    setProfileId(id: number): Challenge {
        this.profileId = id;
        return this;
    }

    setId(id: number): Challenge {
        this.id = id;
        return this;
    }

    setFacebookIds(facebookId: string, facebookPostId: string): Challenge {
        this.facebookId = facebookId;
        this.facebookPostId = facebookPostId;
        return this;
    }
}
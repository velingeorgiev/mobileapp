﻿export class Suggestion {
    public id: number;
    public name: string;
    public completed: boolean;
    public active: boolean;

    setCompleted(): void { // Not used.
        this.completed = true;
    }
}
import { NgModule } from '@angular/core';
import { Http, HttpModule } from '@angular/http';
import { TranslateModule, TranslateLoader, TranslateStaticLoader } from 'ng2-translate/ng2-translate';
import { IonicApp, IonicModule } from 'ionic-angular';

import { Transfer } from 'ionic-native';

import { MyApp } from './app.component';

// Pages.
import { HomePage } from '../pages/home/home';
import { AchievementsPage } from '../pages/achievements/achievements';
import { AchievementPage } from '../pages/achievements/achievement';

import { AlbumPage } from '../pages/albums/album';
import { CategoriesPage } from '../pages/categories/categories';

import { ChallengesPage } from '../pages/challenges/challenges';
import { ChallengePage } from '../pages/challenges/challenge.page';
import { ChallengeForm } from '../pages/challenges/challenge.form';

import { SettingsPage } from '../pages/settings/settings';
import { SkinsPage } from '../pages/skins/skins';

import { ProfilesPage } from '../pages/profiles/profiles';
import { ProfilePage } from '../pages/profiles/profile';

import { GenericComponent } from '../pages/home/generic.component';
import { SelectProfile } from '../pages/profiles/select.profile';

// Query.
import { QueryGenericDataProvider } from './queries/query.generic.data.provider';
import { QueryIndexedDbDataStrategy } from './queries/query.indexed.db.data.strategy';
import { QueryHttpDataStrategy } from './queries/query.http.data.strategy';
import { GenericQuery } from './queries/generic.query';

// Command.
import { CommandGenericDataProvider } from './commands/command.generic.data.provider';
import { CommandIndexedDbDataStrategy } from './commands/command.indexed.db.data.strategy';
import { CommandHttpDataStrategy } from './commands/command.http.data.strategy';
import { PutCommand } from './commands/put.command';
import { PostCommand } from './commands/post.command';
import { PictureTakeCommand } from './commands/picture.take.command';
import { CreateThumbCommand } from './commands/create.thumb.command';
import { PictureCreateCommand } from './commands/picture.create.command';
import { PictureMoveCommand } from './commands/picture.move.command';
import { PictureResizeCommand } from './commands/picture.resize.command';
import { EnsureDirCommand } from './commands/ensure.dir.command';
import { PostChallengeCommand } from './commands/post.challenge.command';
import { FacebookLoginCommand } from './commands/facebook.login.command';
import { FacebookPictureCommand } from './commands/facebook.picture.command';
import { FacebookUploadCommand } from './commands/facebook.upload.command';
import { PictureDeleteCommand } from './commands/picture.delete.command';
import { RemoveCommand } from './commands/remove.command';

// Events.
import { ProfileEvent } from './events/profile.event';
import { ChallengeEvent } from './events/challenge.event';
import { AchievementEvent } from './events/achievement.event';
import { SuggestionEvent } from './events/suggestion.event';

// Subscribers.

// Other.
import { DataList } from './infrastructure/data.list';
import { Context } from './infrastructure/context';
import { IndexedDbDataSeeds } from './commands/indexed.db.data.seeds';
import { AdMobProvider } from './infrastructure/ad.mob.provider';
import { DateProvider } from './infrastructure/date.provider';
import { LoggerProvider } from './infrastructure/logger.provider';

@NgModule({
    declarations: [
        MyApp,
        HomePage,
        AchievementsPage,
        AchievementPage,
        AlbumPage,
        CategoriesPage,
        ChallengesPage,
        ChallengePage,
        ChallengeForm,
        ProfilesPage,
        ProfilePage,
        SettingsPage,
        SkinsPage,
        GenericComponent,
        SelectProfile
    ],
    imports: [
        IonicModule.forRoot(MyApp),
        HttpModule,
        TranslateModule.forRoot({
            provide: TranslateLoader,
            useFactory: (http: Http) => new TranslateStaticLoader(http, 'assets/i18n', '.json'),
            deps: [Http]
        })
    ],
    bootstrap: [IonicApp],
    entryComponents: [
        MyApp,
        HomePage,
        AchievementsPage,
        AchievementPage,
        AlbumPage,
        CategoriesPage,
        ChallengesPage,
        ChallengePage,
        ChallengeForm,
        ProfilesPage,
        ProfilePage,
        SettingsPage,
        SkinsPage,
        GenericComponent,
        SelectProfile
    ],
    providers: [
        Transfer,
        LoggerProvider,
        QueryGenericDataProvider,
        QueryIndexedDbDataStrategy,
        QueryHttpDataStrategy,
        GenericQuery,
        CommandGenericDataProvider,
        CommandIndexedDbDataStrategy,
        CommandHttpDataStrategy,
        PutCommand,
        PostCommand,
        ProfileEvent,
        AchievementEvent,
        SuggestionEvent,
        ChallengeEvent,
        DataList,
        Context,
        IndexedDbDataSeeds,
        AdMobProvider,
        DateProvider,
        PictureTakeCommand,
        CreateThumbCommand,
        PictureCreateCommand,
        PictureMoveCommand,
        PictureResizeCommand,
        EnsureDirCommand,
        PostChallengeCommand,
        FacebookLoginCommand,
        FacebookPictureCommand,
        PictureDeleteCommand,
        FacebookUploadCommand,
        RemoveCommand
    ]
})
export class AppModule { }

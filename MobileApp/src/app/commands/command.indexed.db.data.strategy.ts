﻿import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { CommandDataStrategy } from '../infrastructure/command.data.strategy';

// IndexedDb class implements the data provider will act as offline
// data store when the strategy is set to offline
@Injectable()
export class CommandIndexedDbDataStrategy implements CommandDataStrategy {
    private indexedDB: any;
    private dbName: string;

    constructor() {
        this.indexedDB = indexedDB;
        this.dbName = 'db'; // by default
    }

    setDbName(dbName: string): void {
        if (dbName.length > 0 && dbName !== undefined) {
            this.dbName = dbName;
        }
        else {
            console.log("Error: wrong dbName");
        }
    } //TODO: decide how to handle this better.

    put(source: string, object: any): Observable<any> {
        let self = this;

        return Observable.create((observer: any) => {
            this.open().subscribe((db: any) => {
                let tx = db.transaction(source, "readwrite");
                let store = tx.objectStore(source);
                store.put(object);

                tx.oncomplete = () => {
                    observer.next(object);
                    db.close();
                    observer.complete();
                };
                db.onerror = (e: any) => {
                    db.close();
                    self.handleError("IndexedDB error: " + e.target.errorCode);
                }
            });
        });
    };

    post(source: string, object: any): Observable<any> {
        let self = this;

        return Observable.create((observer: any) => {
            this.open().subscribe((db: any) => {
                let tx = db.transaction(source, "readwrite");
                let store = tx.objectStore(source);
                let request = store.add(object);

                request.onsuccess = (e: any) => {
                    observer.next(e.target.result);
                    db.close();
                    observer.complete();
                }
                db.onerror = (e: any) => {
                    db.close();
                    self.handleError("IndexedDB error: " + e.target.errorCode);
                }
            });
        });
    };

    remove(source: string, id: number): Observable<any> {
        let self = this;

        return Observable.create((observer: any) => {
            this.open().subscribe((db: any) => {
                let tx = db.transaction(source, "readwrite");
                let store = tx.objectStore(source);

                store.delete(id);

                tx.oncomplete = (e: any) => {
                    observer.next(id);
                    db.close();
                    observer.complete();
                };
                db.onerror = (e: any) => {
                    db.close();
                    self.handleError("IndexedDB error: " + e.target.errorCode);
                }
            });
        });
    };

    private handleError(msg: string) {
        console.error(msg);
        return Observable.throw(msg);
    }

    private open(): Observable<any> {
        let self = this;

        return Observable.create((observer: any) => {
            let request = self.indexedDB.open(self.dbName);

            request.onsuccess = () => {
                observer.next(request.result);
                observer.complete();
            }
            request.onerror = () => {
                self.handleError(request.error);
            }
        });
    }
}
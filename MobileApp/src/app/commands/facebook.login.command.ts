﻿import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';

import { Facebook } from 'ionic-native';

import { Command } from '../infrastructure/command';
import { LoggerProvider } from '../infrastructure/logger.provider';

@Injectable()
export class FacebookLoginCommand implements Command {
    
    constructor(
        private logger: LoggerProvider
    ) {}
     
    execute(source: string, object?: any): Observable<string> {
        let self = this;

        this.logger.info(`FacebookLoginCommand.execute|source: ${source}, object: ${JSON.stringify(object)}.`);

        return Observable.create((observer: any) => {

            Facebook.getLoginStatus()
                .then(result => {

                    if (result.status !== "connected") {

                        self.login().subscribe((token: string) => {

                            self.logger.info(`FacebookLoginCommand.execute|getLoginStatus|success|source: ${source}, token: ${token}.`);

                            observer.next(token);
                            observer.complete();
                        });

                    } else {

                        self.logger.info(`FacebookLoginCommand.execute|getLoginStatus|success|source: ${source}, result.authResponse: ${JSON.stringify(result.authResponse)}.`);

                        observer.next(result.authResponse.accessToken);
                        observer.complete();
                    }
                })
                .catch(error => {

                    self.logger.error(`FacebookLoginCommand.execute|getLoginStatus|source: ${source}, error: ${JSON.stringify(error)}.`);
                    Observable.throw("Could not login to facebook.");
                });           
        });
    };

    private login(): Observable<string> {
        let self = this;

        return Observable.create(observer => {

            Facebook.login(["public_profile", "user_photos"])
                .then(result => {

                    self.logger.info(`FacebookLoginCommand.execute|login|params: "public_profile", "user_photos"|success|result: ${JSON.stringify(result)}.`);

                    Facebook.login(["publish_actions"])
                        .then(result2 => {

                            self.logger.info(`FacebookLoginCommand.execute|login|params: "publish_actions"|success|result2: ${JSON.stringify(result2)}.`);

                            Facebook.getAccessToken()
                                .then(token => {

                                    self.logger.info(`FacebookLoginCommand.execute|getAccessToken|success|token: ${token}.`);

                                    observer.next(token);
                                    observer.complete();
                                })
                                .catch(error3 => {

                                    self.logger.error(`FacebookLoginCommand.execute|getAccessToken|error: ${JSON.stringify(error3)}.`);
                                    Observable.throw("Could not login to facebook.");
                                });
                        })
                        .catch(error2 => {

                            self.logger.error(`FacebookLoginCommand.execute|login|params: "publish_actions"|error2: ${JSON.stringify(error2)}.`);
                            Observable.throw("Could not login to facebook.");
                        });
                })
                .catch(error => {

                    self.logger.error(`FacebookLoginCommand.execute|login|params: "public_profile", "user_photos"|error: ${JSON.stringify(error)}.`);
                    Observable.throw("Could not login to facebook.");
                });
        });
    }
}

// NEED a hash key https://developers.facebook.com/docs/android/getting-started - Create a Development Key Hash
// Setting a Release Key Hash - https://developers.facebook.com/docs/android/getting-started#release-key-hash
// https://github.com/jeduan/cordova-plugin-facebook4/blob/master/docs/android/README.md
﻿import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';

import { Command } from '../infrastructure/command';
import { Directory } from '../domains/directory';
import { LoggerProvider } from '../infrastructure/logger.provider';
import { EnsureDirCommand } from './ensure.dir.command';
import { PictureResizeCommand } from './picture.resize.command';

@Injectable()
export class CreateThumbCommand implements Command {

    constructor(
        private pictureResize: PictureResizeCommand,
        private ensureDir: EnsureDirCommand,
        private logger: LoggerProvider
    ) {}

    execute(source: string, imageFullPath: string): Observable<string> {

        this.logger.info(`CreateThumbCommand.execute|source: ${source}, imageFullPath: ${imageFullPath}.`);

        return Observable.create(observer => {

            let thumbsDirName = "thumbs"; // Will this ever change?

            this.ensureDir.execute(source, Directory.create(thumbsDirName)).subscribe(resultDir => {

                let options = PictureResizeCommand.quickSetOptions(imageFullPath, resultDir.fullPath);

                this.pictureResize.execute(source, options).subscribe(thumbUrl => {
                    
                    this.logger.info(`CreateThumbCommand.execute|success|source: ${source}, thumbUrl: ${thumbUrl}.`);
                    observer.next(thumbUrl);
                    observer.complete();
                });
            });
        });
    };
}
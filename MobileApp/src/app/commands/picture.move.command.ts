﻿import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { File, Entry } from 'ionic-native';

import { Command } from '../infrastructure/command';
import { PictureMoveInfo } from '../domains/picture';
import { EnsureDirCommand } from './ensure.dir.command';
import { LoggerProvider } from '../infrastructure/logger.provider';

@Injectable()
export class PictureMoveCommand implements Command {

    constructor(
        private enshureDir: EnsureDirCommand,
        private logger: LoggerProvider
    ) {}

    execute(source: string, info: PictureMoveInfo): Observable<string> {

        this.logger.info(`PictureMoveCommand.execute|source: ${source}, info: ${JSON.stringify(info)}.`);

        return Observable.create(observer => {
            this.enshureDir.execute(source, info.profileDir).subscribe(resultDir => {

                this.logger.info(`PictureMoveCommand.execute|attempt to moveFile to ${resultDir.fullPath}.`);
                
                let path = info.picture.parentDirPath;
                let name = info.picture.name;

                File.moveFile(path, name, resultDir.fullPath, name)
                    .then((entry: Entry) => {

                        this.logger.info(`PictureMoveCommand.execute|moveFile|success|source: ${source}, ${JSON.stringify(entry)}.`);
                        observer.next(entry.nativeURL);
                        observer.complete();
                    })
                    .catch(err => {

                        this.logger.error(`PictureMoveCommand.execute|moveFile|error: ${err.message}, source: ${source}.`);
                        return Observable.throw(`Could not move picture file.`);
                    });
            });
        });
    };
}
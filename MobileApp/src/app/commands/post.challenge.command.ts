﻿import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';

import { Command } from '../infrastructure/command';
import { Challenge } from '../domains/challenge';
import { CommandGenericDataProvider } from './command.generic.data.provider';
import { Profile } from '../domains/profile';
import { Context } from '../infrastructure/context';
import { PictureCreateCommand } from './picture.create.command';
import { LoggerProvider } from '../infrastructure/logger.provider';
import { ChallengePage } from '../../pages/challenges/challenge.page';

@Injectable()
export class PostChallengeCommand implements Command {

    constructor(
        private dataProvider: CommandGenericDataProvider,
        private logger: LoggerProvider,
        private context: Context,
        private createPicture: PictureCreateCommand
    ) {}

    execute(source: string, challenge: Challenge): Observable<Challenge> {
        let self = this;

        this.logger.info(`PostChallengeCommand.execute|source: ${source}, challenge: ${JSON.stringify(challenge)}.`);

        return Observable.create(observer => {

            let profile = this.context.getActiveProfile() as Profile;

            this.createPicture.execute(challenge.name, profile.name)
                .subscribe(picture => {

                    challenge.setPicture(picture).setProfileId(profile.id);

                    this.dataProvider.post("challenges", challenge).subscribe(id => {

                        challenge.setId(id);

                        self.logger.info(`PostChallengeCommand.execute|success|source: ${source}, challenge: ${JSON.stringify(challenge)}.`);

                        observer.next(challenge);
                        observer.complete();
                    });
                });
        });
    };
}
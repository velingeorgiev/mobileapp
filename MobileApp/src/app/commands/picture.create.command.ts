﻿import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';

import { Command } from '../infrastructure/command';
import { Picture, PictureMoveInfo } from '../domains/picture';
import { LoggerProvider } from '../infrastructure/logger.provider';
import { PictureTakeCommand } from './picture.take.command';
import { PictureMoveCommand } from './picture.move.command';
import { CreateThumbCommand } from './create.thumb.command';


@Injectable()
export class PictureCreateCommand implements Command {

    constructor(
        private takePicture: PictureTakeCommand,
        private movePicture: PictureMoveCommand,
        private createThumb: CreateThumbCommand,
        private logger: LoggerProvider
    ) {}

    execute(source: string, profileDirName: string): Observable<Picture> {

        this.logger.info(`PictureCreateCommand.execute|source: ${source}, profileDirName: ${profileDirName}.`);

        return Observable.create(observer => {

            // Take the picture via Camera plugin.
            this.takePicture.execute(source)
                .subscribe((pictureFullPath: string) => {

                    // Move the picture via File plugin to the externalDataDirectory.
                    this.movePicture.execute(source, PictureMoveInfo.create(pictureFullPath, profileDirName))
                        .subscribe((movedPictureFullPath: string) => {

                            // Create thumb and move it to the externalDataDirectory via ImageResize and File plugins.
                            this.createThumb.execute(source, movedPictureFullPath)
                                .subscribe((thumbFullPath: string) => {

                                    let picture = Picture.create(movedPictureFullPath, thumbFullPath);
                                    this.logger.info(`PictureCreateCommand.execute|success|source: ${source}, picture: ${JSON.stringify(picture)}.`);

                                    observer.next(picture);
                                    observer.complete();
                                });
                        });
                });
        });
    };
}
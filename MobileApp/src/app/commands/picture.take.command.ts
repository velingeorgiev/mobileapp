﻿import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { Camera, CameraOptions } from 'ionic-native';

import { Command } from '../infrastructure/command';
import { LoggerProvider } from '../infrastructure/logger.provider';

@Injectable()
export class PictureTakeCommand implements Command {

    constructor(
        private logger: LoggerProvider
    ) {}

    execute(source: string, options?: CameraOptions): Observable<any> {

        this.logger.info(`PictureTakeCommand.execute|source: ${source}, options: ${JSON.stringify(options)}.`);

        if (options === undefined)
            options = PictureTakeCommand.getDafaultOptions();

        return Observable.create(observer => {

            Camera.getPicture(options)
                .then(imageUrl => {

                    this.logger.info(`PictureTakeCommand.execute|getPicture|success|imageUrl: ${imageUrl}.`);
                    observer.next(imageUrl);
                    observer.complete();
                })
                .catch(error => {

                    this.logger.error(`PictureTakeCommand.execute|error: ${JSON.stringify(error)}, source: ${source}, options: ${JSON.stringify(options)}.`);
                    return Observable.throw("Could not take picture.");
                });
        });
    };

    static getDafaultOptions(): CameraOptions {
        return {
            quality: 90,
            destinationType: Camera.DestinationType.FILE_URI,
            pictureSourceType: Camera.PictureSourceType.CAMERA,
            allowEdit: false
        } as CameraOptions;
    };
}
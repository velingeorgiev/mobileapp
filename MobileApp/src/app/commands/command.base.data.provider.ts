﻿import { Observable } from 'rxjs/Observable';
import { CommandDataStrategy } from '../infrastructure/command.data.strategy';
import { CommandDataProvider } from '../infrastructure/command.data.provider';

export abstract class CommandBaseDataProvider implements CommandDataProvider {
    private strategy: CommandDataStrategy;

    setStrategy(strategy: CommandDataStrategy): void {
        this.strategy = strategy;
    }

    put(source: string, object: any): Observable<any> {
        return this.strategy.put(source, object);
    };

    post(source: string, object: any): Observable<any> {
        return this.strategy.post(source, object);
    };

    remove(source: string, id: number): Observable<any> {
        return this.strategy.remove(source, id);
    };
}
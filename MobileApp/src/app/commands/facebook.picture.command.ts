﻿import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';

import { Transfer, FileUploadOptions, FileUploadResult } from 'ionic-native';
import { Challenge } from '../domains/challenge';

import { Command } from '../infrastructure/command';
import { FacebookLoginCommand } from './facebook.login.command';
import { LoggerProvider } from '../infrastructure/logger.provider';
import { DateProvider } from '../infrastructure/date.provider';

@Injectable()
export class FacebookPictureCommand implements Command {

    constructor(
        private logger: LoggerProvider,
        private fileTransfer: Transfer,
        private fbLogin: FacebookLoginCommand
    ) {}
     
    execute(source: string, challenge: Challenge): Observable<string> {
        let self = this;

        this.logger.info(`FacebookPictureCommand.execute|source: ${source}, object: ${JSON.stringify(challenge)}.`);

        return Observable.create(observer => {

            self.fbLogin.execute(source).subscribe(token => {

                self.fileTransfer.upload(challenge.pictureUrl, "https://graph.facebook.com/v2.7/me/photos", self.getUploadOptions(challenge, token))
                    .then((result: FileUploadResult) => {

                        self.logger.info(`FacebookPictureCommand.execute|success|source: ${source}, result: ${JSON.stringify(result)}.`);
                        
                        observer.next(result.response);
                        observer.complete();
                    })
                    .catch(error => {

                        self.logger.error(`FacebookPictureCommand.execute|source: ${source}, error: ${JSON.stringify(error)}.`);
                        Observable.throw("Could not upload picture to facebook.");
                    });
            });
        });
    };

    private getUploadOptions(callenge: Challenge, token: string): FileUploadOptions {

        let options = {
            fileKey: "file",
            fileName: `${callenge.name}_${DateProvider.getTime()}`,
            mimeType: "image/jpg",
            params: {
                access_token: token,
                caption: callenge.memory,
                backdated_time: DateProvider.now(),
                no_story: false
            }
        } as FileUploadOptions;

        this.logger.info(`FacebookPictureCommand.execute|getUploadOptions|options: ${JSON.stringify(options)}.`);

        return options;
    }
}
﻿import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';

import { Challenge } from '../domains/challenge';
import { Command } from '../infrastructure/command';
import { FacebookPictureCommand } from './facebook.picture.command';
import { LoggerProvider } from '../infrastructure/logger.provider';
import { PutCommand } from '../commands/put.command';

@Injectable()
export class FacebookUploadCommand implements Command {

    constructor(
        private logger: LoggerProvider,
        private facebookPictureCommand: FacebookPictureCommand,
        private putCommand: PutCommand
    ) { }

    execute(source: string, challenge: Challenge): Observable<boolean> {
        let self = this;

        this.logger.info(`FacebookUploadCommand.execute|source: ${source}, challenge: ${JSON.stringify(challenge)}.`);

        return Observable.create(observer => {

            this.facebookPictureCommand
                .execute("ChallengePage", challenge)
                .subscribe(response => {
                    let data = JSON.parse(response);
                    challenge.setFacebookIds(data.id, data.post_id);

                    self.putCommand.execute("challenges", challenge)
                        .subscribe(resultChallenge => {

                            this.logger.info(`FacebookUploadCommand.execute|success|source: ${source}, resultChallenge: ${JSON.stringify(resultChallenge)}.`);

                            observer.next(true);
                            observer.complete();
                        });
                });
        });
    };
}
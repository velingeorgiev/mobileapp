﻿import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { File } from 'ionic-native';

import { Directory } from '../domains/directory';
import { Command } from '../infrastructure/command';
import { LoggerProvider } from '../infrastructure/logger.provider';

@Injectable()
export class EnsureDirCommand implements Command {
    
    constructor(
        private logger: LoggerProvider
    ) {}

    execute(source: string, dir: Directory): Observable<Directory> {

        this.logger.info(`EnsureDirCommand.execute|source: ${source}, dir: ${JSON.stringify(dir)}.`);

        return Observable.create((observer: any) => {

            File.checkDir(dir.parentDir, dir.name)
                .then(exists => {
                    this.logger.info(`EnsureDirCommand.execute|checkDir|dirPath: ${dir.fullPath} exists.`);
                    observer.next(dir);
                    observer.complete(); 
                })
                .catch(err => {
                    this.logger.info(`EnsureDirCommand.execute|checkDir|error: ${err.message}.`);
                    this.logger.info(`EnsureDirCommand.execute|checkDir|dirPath: ${dir.fullPath} does not exist. Attempt to create new dir.`);

                    File.createDir(dir.parentDir, dir.name, false).then(res => {
                        
                        this.logger.info(`EnsureDirCommand|createDir|success.`);
                        observer.next(dir);
                        observer.complete();
                    }).catch(err => {

                        this.logger.error(`EnsureDirCommand.execute|createDir|failed|error: ${err.message}.`);
                        this.logger.error(`EnsureDirCommand.execute|createDir|source: ${source}, dir: ${JSON.stringify(dir)}.`);
                        return Observable.throw(`Failed to create profile directory.`);
                    });
                });
        });
    };
}
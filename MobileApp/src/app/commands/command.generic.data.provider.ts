﻿import { Injectable } from '@angular/core';
import { Context } from '../infrastructure/context';
import { CommandBaseDataProvider } from './command.base.data.provider';
import { CommandIndexedDbDataStrategy } from './command.indexed.db.data.strategy';
import { CommandHttpDataStrategy } from './command.http.data.strategy';

@Injectable()
export class CommandGenericDataProvider extends CommandBaseDataProvider {

    constructor(
        private httpStrategy: CommandHttpDataStrategy,
        private indexedDbStrategy: CommandIndexedDbDataStrategy,
        private context: Context
    ) {
        super();
        this.setStrategy(indexedDbStrategy); // by default
        
        let strategy = this.context.get("dataStrategy");

        if (strategy !== undefined && strategy === "online") {

            this.setStrategy(httpStrategy);
        }
    }
}
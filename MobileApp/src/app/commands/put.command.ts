﻿import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';

import { Command } from '../infrastructure/command';
import { CommandGenericDataProvider } from './command.generic.data.provider';
import { LoggerProvider } from '../infrastructure/logger.provider';

@Injectable()
export class PutCommand implements Command {

    constructor(
        private dataProvider: CommandGenericDataProvider,
        private logger: LoggerProvider
    ) { }

    execute(source: string, object: any): Observable<any> {

        this.logger.info(`PutCommand.execute|source: ${source}, object: ${JSON.stringify(object)}.`);

        return Observable.create((observer: any) => {

            this.dataProvider.put(source, object).subscribe(result => {

                this.logger.info(`PutCommand.execute|success|source: ${source}, result: ${JSON.stringify(result)}.`);

                observer.next(result);
                observer.complete();
            });
        });
    };
}
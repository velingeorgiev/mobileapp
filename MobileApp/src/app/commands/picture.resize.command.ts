﻿import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { ImageResizer, ImageResizerOptions } from 'ionic-native';

import { Command } from '../infrastructure/command';
import { LoggerProvider } from '../infrastructure/logger.provider';

@Injectable()
export class PictureResizeCommand implements Command {
    
    constructor(
        private logger: LoggerProvider
    ) {}

    execute(source: string, options: ImageResizerOptions): Observable<string> {

        this.logger.info(`PictureResizeCommand.execute|source: ${source}, options: ${JSON.stringify(options)}.`);

        return Observable.create((observer: any) => {
            ImageResizer.resize(options)
                .then(resizedImageUrl => {

                    this.logger.info(`PictureResizeCommand.execute|success|source: ${source}, resizedImageUrl: ${resizedImageUrl}.`);
                    observer.next(resizedImageUrl);
                    observer.complete();
                })
                .catch(error => {

                    this.logger.info(`PictureResizeCommand.execute|error: ${error.message}, source: ${source}.`);
                    return Observable.throw(`Could not resize thumbnail.`);
                });
        });
    };

    static quickSetOptions(imageUri: string, dirFullPath?: string): ImageResizerOptions {

        let options = {
            uri: imageUri,
            quality: 80,
            width: 200,
            height: 200
        } as ImageResizerOptions

        if (dirFullPath)
            options.folderName = dirFullPath;

        return options;
    }
}
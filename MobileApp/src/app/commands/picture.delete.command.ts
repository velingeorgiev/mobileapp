﻿import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { File, RemoveResult } from 'ionic-native';

import { Command } from '../infrastructure/command';
import { Picture } from '../domains/picture';
import { EnsureDirCommand } from './ensure.dir.command';
import { LoggerProvider } from '../infrastructure/logger.provider';

@Injectable()
export class PictureDeleteCommand implements Command {

    constructor(
        private enshureDir: EnsureDirCommand,
        private logger: LoggerProvider
    ) {}

    execute(source: string, picture: Picture): Observable<boolean> {

        this.logger.info(`PictureDeleteCommand.execute|source: ${source}, info: ${JSON.stringify(picture)}.`);

        return Observable.create(observer => {

            this.logger.info(`PictureDeleteCommand.execute|attempt to removeFile to ${picture.fullPath}.`);

            File.removeFile(picture.parentDirPath, picture.name)
                .then((result: RemoveResult) => {

                    this.logger.info(`PictureDeleteCommand.execute|removeFile|success|source: ${source}, RemoveResult: ${JSON.stringify(result)}.`);
                    observer.next(result.success);
                    observer.complete();
                })
                .catch(err => {

                    this.logger.error(`PictureDeleteCommand.execute|removeFile|error: ${err.message}, source: ${source}.`);
                    return Observable.throw(`Could not delete picture file.`);
                });
        });
    };
}
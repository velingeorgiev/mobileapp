﻿import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';

import { Command } from '../infrastructure/command';
import { CommandGenericDataProvider } from './command.generic.data.provider';
import { LoggerProvider } from '../infrastructure/logger.provider';

@Injectable()
export class PostCommand implements Command {

    constructor(
        private dataProvider: CommandGenericDataProvider,
        private logger: LoggerProvider
    ) {}
     
    execute(source: string, object: any): Observable<any> {

        this.logger.info(`PostCommand.execute|source: ${source}, object: ${JSON.stringify(object)}.`);

        return Observable.create((observer: any) => {

            this.dataProvider.post(source, object).subscribe(id => {

                this.logger.info(`PostCommand.execute|success|source: ${source}, object: ${JSON.stringify(object)}, id:${id}.`);

                observer.next(id);
                observer.complete();
            });
        });
    };
}
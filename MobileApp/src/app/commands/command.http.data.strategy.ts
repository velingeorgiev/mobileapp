﻿import { Http, Response } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/catch';
import { CommandDataStrategy } from '../infrastructure/command.data.strategy';
import { Injectable } from '@angular/core';

// Http class implements the data provider will act as offline
// data store when the strategy is set to offline
@Injectable()
export class CommandHttpDataStrategy implements CommandDataStrategy {

    constructor(private http: Http) {}

    put(source: string, object: any): Observable<any> {
        return this.http.post(source, object)
            .map(this.extractData)
            .catch(this.handleError);
    };

    post(source: string, object: any): Observable<any> {
        return this.http.post(source, object)
            .map(this.extractData)
            .catch(this.handleError);
    };

    remove(source: string, id: number): Observable<any> {
        return this.http.delete(source, id)
            .map(this.extractData)
            .catch(this.handleError);
    };

    private extractData(res: Response) {
        let body = res.json();
        return body.data || {};
    }

    private handleError(error: any) {
        // TODO: Refactor add logging
        // In a real world app, we might use a remote logging infrastructure
        let errMsg = (error.message) ? error.message :
            error.status ? `${error.status} - ${error.statusText}` : 'Server error';
        console.error(errMsg); // log to console instead
        return Observable.throw(errMsg);
    }
}
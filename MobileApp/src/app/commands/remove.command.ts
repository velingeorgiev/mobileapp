﻿import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';

import { Command } from '../infrastructure/command';
import { CommandGenericDataProvider } from './command.generic.data.provider';
import { LoggerProvider } from '../infrastructure/logger.provider';

@Injectable()
export class RemoveCommand implements Command {

    constructor(
        private dataProvider: CommandGenericDataProvider,
        private logger: LoggerProvider
    ) { }

    execute(source: string, id: number): Observable<any> {

        this.logger.info(`RemoveCommand.execute|source: ${source}, id: ${JSON.stringify(id)}.`);

        return Observable.create((observer: any) => {

            this.dataProvider.remove(source, id).subscribe(result => {

                this.logger.info(`RemoveCommand.execute|success|source: ${source}, result: ${JSON.stringify(result)}.`);

                observer.next(result);
                observer.complete();
            });
        });
    };
}
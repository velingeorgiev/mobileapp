﻿import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';

import { CommandClientDataProvider } from '../infrastructure/command.client.data.provider';

@Injectable()
export class IndexedDbDataSeeds implements CommandClientDataProvider {
    private schema: any[];
    private indexedDB: any;
    private dbName: string;

    constructor() {
        // Set schema to the default schema.
        this.indexedDB = indexedDB;
        this.dbName = "db"; // by default
        this.schema = this.getDefaultSchema();
    }

    setDbName(name: string): void {
        if (name.length > 0 && name !== undefined) {
            this.dbName = name;
        }
        else {
            console.log("Error: wrong dbName");
        }
    } //TODO: decide weather to put into interface.

    setDbSchema(schema: any[]): void {
        this.schema = schema;
    } //TODO: decide weather to put into interface.

    seed(dbOverwrite?: boolean): void {
        let success = undefined;
        if (dbOverwrite) {
            this.clear().subscribe(done => this.create(this.schema).subscribe(done => success));
        } else {
            this.create(this.schema).subscribe(done => success);
        }
    } //TODO: decide weather to put into interface.

    create(schema: any[]): Observable<any> {
        let self = this;

        return Observable.create((observer: any) => {
            let request = self.indexedDB.open(self.dbName);

            request.onupgradeneeded = () => {
                // The database did not previously exist, so create object stores and indexes.
                let db = request.result;

                for (let i = 0; i < schema.length; i++) {
                    let store = db.createObjectStore(schema[i].name, { keyPath: "id", autoIncrement: true });
                    store.createIndex("id_idx", "id", { unique: true });

                    if (schema[i].indexes !== undefined) {
                        for (let j = 0; j < schema[i].indexes.length; j++) {
                            let index = schema[i].indexes[j];
                            store.createIndex(`${index}_idx`, index);
                        }
                    }

                    if (schema[i].seeds !== undefined) {
                        for (let j = 0; j < schema[i].seeds.length; j++) {
                            let seed = schema[i].seeds[j];
                            store.put(seed);
                        }
                    }
                }
            };

            request.onerror = () => {
                self.handleError(request.error);
            }

            request.onsuccess = () => {
                let db = request.result;
                db.close();
                observer.next('done');
                observer.complete();
            }
        });
    }

    clear(): Observable<any> {
        let self = this;

        return Observable.create((observer: any) => {
            let request = self.indexedDB.deleteDatabase(self.dbName);

            request.onsuccess = () => {
                observer.next('done');
                observer.complete();
            }
            request.onerror = () => {
                self.handleError('Could not delete indexed db.');
            };
            request.onblocked = () => {
                self.handleError('Couldn not delete database due to the operation being blocked.');
            };
        });
    }

    getDefaultSchema(): any[] {

        let profile = { id: 1, created: "2016-07-14T07:48:03.374Z", modified: "2016-07-14T07:48:03.374Z", name: "Borko", isActive: true };

        // Seed the localstorage.
        localStorage.setItem('profile', JSON.stringify(profile));

        return [
            {
                name: 'profiles',
                indexes: ['name'],
                seeds: [profile]
            },
            {
                name: 'categories',
                indexes: ['name', 'type'],
                seeds: [
                    { id:1, name: "Cat_Pregnancy", type: 1, active: true },
                    { id:2, name: "Cat_Gifts", type: 0, active: true }
                ]
            },
            {
                name: 'suggestions',
                indexes: ['categoryId', 'name'],
                seeds: [
                    { categoryId: 1, name: "Ch_pregnancy_test", active: true, completed: true },
                    { categoryId: 1, name: "Ch_first_month", active: true, completed: false },
                    { categoryId: 1, name: "Ch_second_month", active: true, completed: false },
                    { categoryId: 1, name: "Ch_third_month", active: true, completed: false },
                    { categoryId: 1, name: "Ch_fourth_month", active: true, completed: false },
                    { categoryId: 1, name: "Ch_fifth_month", active: true, completed: false },
                    { categoryId: 1, name: "Ch_sixth_month", active: true, completed: false },
                    { categoryId: 1, name: "Ch_seventh_month", active: true, completed: false },
                    { categoryId: 1, name: "Ch_eighth_month", active: true, completed: false },
                    { categoryId: 1, name: "Ch_ninth_month", active: true, completed: false }
                ]
            },
            {
                name: 'challenges',
                indexes: ['categoryId', 'name', 'profileId'],
                seeds: [{
                    name: "Ch_pregnancy_test",
                    created: "2016-07-14T07:48:03.374Z",
                    modified: "2016-07-14T07:48:03.374Z",
                    pictureUrl: "https://lh5.googleusercontent.com/-v2FmHeGkPxU/VcCW7CILGeI/AAAAAAAACJc/oLe9abDcOvckV2X-tAU-l3jZvZsB0fA_QCL0B/s459-no/6e2jwf7ztbr5t1yjq4c5.jpeg",
                    profileId: 1,
                    categoryId: 1,
                    memory: "Moment of love"
                }]
            },
            {
                name: 'achievements',
                indexes: ['name'],
                seeds: [
                    { name: "achi_first_contact", count: 0, target: 1 },
                    { name: "achi_second", count: 0, target: 1 },
                    { name: "achi_third", count: 0, target: 30 },
                    { name: "achi_fourth", count: 0, target: 50 }
                ]
            }
        ];
    } //TODO: decide weather to put into interface.

    private handleError(msg: string) {
        console.error(msg);
        return Observable.throw(msg);
    }
}
﻿import { Injectable } from '@angular/core';
import { Context } from './context';
import { AdMob } from 'ionic-native';

// App Context using localStorage. 
// Can store all app settings including current active profile, current language etc.
@Injectable()
export class AdMobProvider {
    private adMob: any;
     
    constructor(private context: Context) {
        this.adMob = AdMob;
    }

    createBanner(): void {
        let options = {
            adId: this.context.getAdMobBannerId(),
            position: this.adMob.BOTTOM_CENTER,
            autoShow: true,
            isTesting: true,
            overlap: false,
            offsetTopBar: false,
            bgColor: 'black'
        };
        this.adMob.createBanner(options);
    }

    prepareInterstitial(): void {
        let options = {
            adId: this.context.getAdMobinterstitialId(),
            autoShow: false
        };
        this.adMob.prepareInterstitial(options);
    }

    showInterstitial(): void {
        this.adMob.showInterstitial();
    }
}
﻿import { OnInit } from '@angular/core';

export abstract class MultiScreenPage implements OnInit  {
    private nav: string[];
    protected currentScreen: string;

    abstract ngOnInit(): void;

    constructor() {
        this.nav = [];
    }

    protected setScreen(nextScreen: string): void {
        this.currentScreen = nextScreen;
        this.nav.push(nextScreen);
    }

    protected backScreen(): void { 
        this.nav.pop(); // Current.
        let back = this.nav.pop(); // Back.
        this.setScreen(back);
    }
}
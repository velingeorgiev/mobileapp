﻿import { Observable } from 'rxjs/Observable';

// The client data providers are related to the browser storages
// like indexedDb, localStorages. We need to provide 'create' and 'clear' methods
// for them so we can seed the storages or remove them at all.
export interface CommandClientDataProvider {

    // Different store initialization depending on strategy.
    // The Http might not have such, but IndexedDB requires
    // initialization of the stores if they do not exist.
    create(schema?: any[]): Observable<any>;

    // Clear the database.
    clear(): Observable<any>;
}
﻿// To be implemented by page components where action is required.
export interface Page {

    onSubmit(object?: any): void;

}
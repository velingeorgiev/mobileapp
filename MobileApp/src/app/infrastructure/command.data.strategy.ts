﻿import { Observable } from 'rxjs/Observable';

// Defines strategies to be used in the command data provider.
// Idealy the app might have many strategies and depending on 
// what strategy is set in the data provider, the flow of data 
// will be delivered by different source like http, indexedDb, localStorage or other.
// See the data provider for more info.
export interface CommandDataStrategy {

    // Executes http put request or update in data collection
    put(source: string, object: any): Observable<any>;

    // Executes http post request or insertion in data collection.
    post(source: string, object: any): Observable<any>;

    // Executes http delete request or deletion in data collection.
    remove(source: string, id: number): Observable<any>;
}
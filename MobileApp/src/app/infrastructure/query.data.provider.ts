﻿import { Observable } from 'rxjs/Observable';
import { QueryDataStrategy } from './query.data.strategy';

// The mother of all data providers. Used to interact with data sources like
// http, indexedDb, localStorage or other.
// Controls pretty much all the flow of data that comes in or goes out the app.
export interface QueryDataProvider {

    setStrategy(strategy: QueryDataStrategy): void;

    getById(source: string, id: number): Observable<any>;

    get(source: string, filter: any): Observable<any>;

    all(source: string, filter?: any): Observable<any[]>;

    count(source: string): Observable<any>;
}
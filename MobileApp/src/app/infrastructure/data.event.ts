﻿import { EventEmitter } from '@angular/core';

// Data related pub/sub event can implement this interface.
// Example: Profile can have add, updated, deleted events
// emitted and a Profile list can subscribe to these events and
// perform list updates on the client based on the event emitted.
export interface DataEvent {
    onAdded: EventEmitter<any>;
    onUpdated: EventEmitter<any>;
    onDeleted: EventEmitter<any>;

    emitAdded(object: any, objectId: any): void;
    emitUpdated(object: any): void;
    emitDeleted(object: any): void;
}
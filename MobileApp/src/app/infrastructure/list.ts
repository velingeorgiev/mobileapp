﻿// Does inplace manipulation of arrays.
// Used when item is added, updated or deleted on the server and as a result
// client array should be updated. So the List helps doing client update of
// array with data instead calling the server to get the refreshed list.
export interface List {

    add(array: any[], object: any, objectKeyValue: any, objectKey?: string): void;

    update(array: any[], object: any, objectKey?: string): void;

    remove(array: any[], object: any, objectKey?: string): void;
}
﻿import { Injectable } from '@angular/core';
import { List } from './list';

// Does in place manipulation of data entities.
// Used to offline update array wtih server data
// from which entity is added, updated or removed.
@Injectable()
export class DataList implements List {

    add(array: any[], object: any, objectKeyValue: any, objectKey?: string): void {

        if (objectKey === undefined) objectKey = "id";

        object[objectKey] = objectKeyValue;
        array.push(object);
    };

    update(array: any[], object: any, objectKey?: string): void {

        if (objectKey === undefined) objectKey = "id";

        array.forEach(
            (item, i) => {
                if (item[objectKey] === object[objectKey]) {
                    array[i] = object;
                };
            }
        );
    }; 

    remove(array: any[], object: any, objectKey?: string): void {

        if (objectKey === undefined) objectKey = "id";

        array.forEach(
            (item, i) => {
                if (item[objectKey] === object[objectKey]) {
                    array.splice(i, 1);
                };
            }
        );
    };
}
﻿import { Observable } from 'rxjs/Observable';

// Used by all list or other components displaying data. 
export interface Query {

    // Returns Observable with object from source like db collection, http request
    getById(id: number): Observable<any>;

    // Returns Observable with object from source like db collection, http request
    get(filter?: any): Observable<any>;

    // Returns Observable with all objects from source like db collection, http request
    all(filter?: any): Observable<any[]>; 

    // Returns Observable with count from source like db collection, http request
    count(source: string): Observable<any[]>; 
}
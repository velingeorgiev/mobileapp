﻿import { Injectable } from '@angular/core';
import { Logger, LoggerLevel } from './logger';

@Injectable()
export class LoggerProvider implements Logger {
    private w: any;
    private store: any;
    private level: LoggerLevel;
    private isConsoleEnabled: boolean;

    constructor() {
        this.w = window;
        this.store = window.localStorage;

        let level = this.store.getItem("LoggerLevel");
        if (level === undefined) {
            this.level = LoggerLevel.Warn;
        } else {
            this.level = parseInt(level);
        }
        console.log(`App logging level set to: ${this.level}.`);

        let isConsoleEnabled = this.store.getItem("LoggerConsoleEnabled");
        if (level === undefined) {
            this.isConsoleEnabled = false;
        } else {
            this.isConsoleEnabled = (isConsoleEnabled == "true");
        }
        console.log(`App console logging enabled: ${this.isConsoleEnabled}.`);
    }

    enableConsole(): void {
        this.store.setItem("LoggerConsoleEnabled", true);
    }

    setLevel(level: LoggerLevel): void {
        this.store.setItem("LoggerLevel", level);
    }

    info(message: string): void {
        let log = `INFO: ${message}`;
        this.logToConsole(log);
    }

    warn(message: string): void {
        let log = `WARN: ${message}`;
        this.logToConsole(log);
    }

    error(message: string): void {
        let log = `ERROR: ${message}`;
        this.logToConsole(log);
    }

    private logToConsole(object: any): void {
        if (this.isConsoleEnabled) console.log(object);
    }
}

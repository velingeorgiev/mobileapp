﻿import { Observable } from 'rxjs/Observable';
import { CommandDataStrategy } from './command.data.strategy';

// The mother of all data providers. Used to interact with data sources like
// http, indexedDb, localStorage or other.
// Controls pretty much all the flow of data that comes in or goes out the app.
export interface CommandDataProvider {

    setStrategy(strategy: CommandDataStrategy): void;

    put(source: string, object: any): Observable<any>;

    post(source: string, object: any): Observable<any>;

    remove(source: string, id: number): Observable<any>;
}
﻿import { Page } from './page';
import { OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';

// To act as a template
export abstract class Form implements Page, OnInit {
    private formSchema: any;

    protected form: FormGroup;
    protected formErrors: any;
    protected validationMessages: any;

    constructor(
        private fb: FormBuilder
    ) {}

    protected abstract setFormSchema(): any;
    // ex.
    //{
    //    name: ['', Validators.required],
    //    email: ['', Validators.required],
    //}

    protected abstract setFormErrors(): any;
    // ex. {'name': '', 'email': '' };

    protected abstract setValidationMessages(): any;
    //validationMessages ex.
    //{
    //    'name': {
    //        'required': 'name is required.',
    //        'minlength': 'Name must be at least 4 characters long.',
    //        'maxlength': 'Name cannot be more than 24 characters long.',
    //        'forbiddenName': 'Someone named "Bob" cannot be a hero.'
    //    },
    //    'email': {
    //        'required': 'email is required.'
    //    }
    //}; 

    protected abstract init(): void; // pre-hook

    ngOnInit(): void {
        this.init();
        this.formSchema = this.setFormSchema();
        this.formErrors = this.setFormErrors();
        this.validationMessages = this.setValidationMessages();
        this.buildForm();
    }

    private buildForm(): void {
        this.form = this.fb.group(this.formSchema);
        this.form.valueChanges.subscribe(data => this.onValueChanged(data));
        this.onValueChanged(); // (re)set validation messages now
    }

    protected onValueChanged(data?: any): void {
        if (!this.form) { return; }
        const form = this.form;
        for (const field in this.formErrors) {
            // clear previous error message (if any)
            this.formErrors[field] = '';
            const control = form.get(field);
            if (control && control.dirty && !control.valid) {
                const messages = this.validationMessages[field];
                for (const key in control.errors) {
                    this.formErrors[field] += messages[key] + ' ';
                }
            }
        }
    }

    abstract onSubmit(object?: any): void;
}
// See: https://plnkr.co/edit/?p=preview
﻿import { Injectable } from '@angular/core';

@Injectable()
export class DateProvider {

    // Composition for better for unit testing.
    static now(): string {
        return new Date().toJSON();
    }

    static getTime(): number {
        return new Date().getTime();
    }
}
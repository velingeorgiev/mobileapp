﻿import { Observable } from 'rxjs/Observable';

// To be used in all components related to post, put or delete of objects.
export interface Command {

    // Performs command on a single object in collection
    execute(source: string, object: any): Observable<any>;
}
﻿import { Observable } from 'rxjs/Observable';

// Defines strategies to be used in the data provider.
// Idealy the app might have many strategies and depending on 
// what strategy is set in the data provider, the flow of data 
// will be delivered by different source like http, indexedDb, localStorage or other.
// See the data provider for more info.
export interface QueryDataStrategy {

    // Executes http get request or selects entity from data collection.
    getById(source: string, id: number): Observable<any[]>;  

    // Executes http get request or selects entity from data collection.
    get(source: string, filter?: any): Observable<any[]>;

    // Executes http get all request or selects all from data collection.
    // Has optional filter criteria for basic filtering
    all(source: string, filter?: any): Observable<any[]>;

    // Executes http get request or select count in data collection.
    count(source: string): Observable<any>;
}
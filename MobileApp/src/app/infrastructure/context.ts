﻿import { Injectable } from '@angular/core';

// App Context using localStorage. 
// Can store all app settings including current active profile, current language etc.
@Injectable()
export class Context {
    private storage: any;
    private paid: boolean;

    constructor() {
        this.storage = localStorage;
        this.paid = false;
    }

    isPaid(): boolean {
        return this.paid;
    }

    getAdMobBannerId(): string {
        return "ca-app-pub-2060470110052581/6198286558";
    }

    getAdMobinterstitialId(): string {
        return "ca-app-pub-2060470110052581/7675019751";
    }

    getActiveProfile(): any {
        let result = this.storage.getItem("profile");
        if (result !== undefined) {
            return JSON.parse(result);
        }
        return undefined;
    }

    get(key: string): any {
        return this.storage.getItem(key);
    }

    getObject(key: string, object: any): void {
        let result = this.storage.getItem(key);
        return JSON.parse(result);
    }

    set(key: string, object: any): void {
        this.storage.setItem(key, object);
    }

    setObject(key: string, object: any): void {
        this.storage.setItem(key, JSON.stringify(object));
    }

    remove(key: string): void {
        this.storage.removeItem(key);
    }
}
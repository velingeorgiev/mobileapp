﻿import { Output, EventEmitter } from '@angular/core';

import { Category } from '../domains/category';
import { Suggestion } from '../domains/suggestion';
import { Challenge } from '../domains/challenge';

export class AchievementEvent {
    @Output() onIncrementCategory: EventEmitter<Category> = new EventEmitter(true);
    @Output() onIncrementSuggestion: EventEmitter<Suggestion> = new EventEmitter(true);
    @Output() onIncrementChallenge: EventEmitter<Challenge> = new EventEmitter(true);

    emitIncrementCategory(category: Category): void {
        this.onIncrementCategory.emit(category);
    };

    emitIncrementSuggestion(suggestion: Suggestion): void {
        this.onIncrementSuggestion.emit(suggestion);
    };

    emitIncrementChallenge(challenge: Challenge): void {
        this.onIncrementChallenge.emit(challenge);
    };
}
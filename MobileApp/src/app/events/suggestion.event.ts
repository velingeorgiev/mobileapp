﻿import { Output, EventEmitter } from '@angular/core';
import { Suggestion } from '../domains/suggestion';

// See DataEvent interface.
export class SuggestionEvent {
    @Output() onUpdated: EventEmitter<Suggestion> = new EventEmitter(true);
    @Output() onDeleted: EventEmitter<Suggestion> = new EventEmitter(true);

    emitUpdated(suggestion: Suggestion): void {
        this.onUpdated.emit(suggestion);
    };

    emitDeleted(suggestion: Suggestion): void {
        this.onDeleted.emit(suggestion);
    };
}
﻿import { Output, EventEmitter } from '@angular/core';
import { DataEvent } from '../infrastructure/data.event';

// See DataEvent interface.
export abstract class BaseDataEvent implements DataEvent {
    @Output() onAdded: EventEmitter<any> = new EventEmitter(true);
    @Output() onUpdated: EventEmitter<any> = new EventEmitter(true);
    @Output() onDeleted: EventEmitter<any> = new EventEmitter(true);

    emitAdded(object: any): void {
        this.onAdded.emit(object);
    };

    emitUpdated(object: any): void {
        this.onUpdated.emit(object);
    };

    emitDeleted(object: any): void {
        this.onDeleted.emit(object);
    };
}
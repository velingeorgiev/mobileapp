﻿import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';

import { Context } from '../infrastructure/context';
import { QueryBaseDataProvider } from './query.base.data.provider';
import { QueryIndexedDbDataStrategy } from './query.indexed.db.data.strategy';
import { QueryHttpDataStrategy } from './query.http.data.strategy';

@Injectable()
export class QueryGenericDataProvider extends QueryBaseDataProvider {

    constructor(
        private httpStrategy: QueryHttpDataStrategy,
        private indexedDbStrategy: QueryIndexedDbDataStrategy,
        private context: Context
    ) {
        super();

        this.setStrategy(indexedDbStrategy); // by default
        
        let strategy = this.context.get("dataStrategy");

        if (strategy !== undefined && strategy === "online") {

            this.setStrategy(httpStrategy);
        }
    }
}
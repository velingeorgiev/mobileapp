﻿import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';

import { Query } from '../infrastructure/query';
import { LoggerProvider } from '../infrastructure/logger.provider';
import { QueryGenericDataProvider } from './query.generic.data.provider';

@Injectable()
export class GenericQuery implements Query {
    private source: string;
    private noSource: string;

    constructor(
       private dataProvider: QueryGenericDataProvider,
       private logger: LoggerProvider
    ){}

    setSource(source: string) {
        this.source = source;
        this.noSource = "Missing source. This is generic query and setSource method have to be used to define source.";
    }

    getById(id: number): Observable<any> {
        if (this.source === undefined)
            return this.handleError(this.noSource);

        this.logger.info(`GenericQuery.getById(${id}), source: ${this.source}`);

        // Log to event tracer

        return this.dataProvider.getById(this.source, id);

        // Log error if fail
    };

    get(filter?: any): Observable<any[]> {
        if (this.source === undefined)
            return this.handleError(this.noSource);
        return this.dataProvider.get(this.source, filter);
    };

    all(): Observable<any[]> {
        if (this.source === undefined)
            return this.handleError(this.noSource);

        return this.dataProvider.all(this.source);
    };

    count(source: string): Observable<any[]> {
        if (this.source === undefined)
            return this.handleError(this.noSource);

        return this.dataProvider.count(this.source);
    };

    private handleError(error:string) {
        return Observable.throw(error);
    }
}
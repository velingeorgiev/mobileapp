﻿import { Http, Response } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import { QueryDataStrategy } from '../infrastructure/query.data.strategy';
import { Injectable } from '@angular/core';

// Http class implements the data provider will act as offline
// data store when the strategy is set to offline
@Injectable()
export class QueryHttpDataStrategy implements QueryDataStrategy {

    constructor(private http: Http) { }

    getById(source: string, id: number): Observable<any> {
        return this.http.get(`${source}/${id}`)
            .map(this.extractData)
            .catch(this.handleError);
    };

    get(source: string, filter: any): Observable<any> {
        return this.http.get(`${source}/${filter}`)
            .map(this.extractData)
            .catch(this.handleError);
    };

    all(source: string, filter?: any): Observable<any[]> {
        return this.http.get(source)
            .map(this.extractData)
            .catch(this.handleError);
    };

    count(source: string): Observable<any> {
        return this.http.get(source)
            .map(this.extractData)
            .catch(this.handleError);
    };

    private extractData(res: Response) {
        let body = res.json();
        return body.data || {};
    }

    private handleError(error: any) {
        // TODO: Refactor add logging
        // In a real world app, we might use a remote logging infrastructure
        let errMsg = (error.message) ? error.message :
            error.status ? `${error.status} - ${error.statusText}` : 'Server error';
        console.error(errMsg); // log to console instead
        return Observable.throw(errMsg);
    }
}
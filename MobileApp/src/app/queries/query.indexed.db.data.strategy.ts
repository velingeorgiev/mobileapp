﻿import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { QueryDataStrategy } from '../infrastructure/query.data.strategy';

// IndexedDb class implements the data provider will act as offline
// data store when the strategy is set to offline
@Injectable()
export class QueryIndexedDbDataStrategy implements QueryDataStrategy {
    private indexedDB: any;
    private dbName: string;

    constructor() {
        this.indexedDB = indexedDB;
        this.dbName = 'db'; // by default
    }

    setDbName(dbName: string): void {
        if (dbName.length > 0 && dbName !== undefined) {
            this.dbName = dbName;
        }
        else {
            console.log("Error: wrong dbName");
        }
    } //TODO: decide how to handle this better.

    getById(source: string, id: number): Observable<any> {
        let self = this;

        return Observable.create((observer: any) => {
            this.open().subscribe((db: any) => {
                let tx = db.transaction(source, "readonly");
                let store = tx.objectStore(source);
                let index = store.index("id_idx");
                let request = index.get(id);

                request.onsuccess = () => {
                    observer.next(request.result);
                    db.close();
                    observer.complete();
                };
                db.onerror = (e: any) => {
                    db.close();
                    self.handleError("IndexedDB error: " + e.target.errorCode);
                }
            });
        });
    };

    get(source: string, filter: any): Observable<any> {
        let self = this;

        if (filter.index === undefined || filter.value === undefined) {
            return self.handleError("Filter index or value is missing");
        }

        return Observable.create((observer: any) => {
            this.open().subscribe((db: any) => {
                let tx = db.transaction(source, "readonly");
                let store = tx.objectStore(source);
                let index = store.index(filter.index);
                let request = index.openCursor(IDBKeyRange.only(filter.value)); //IDBKeyRange.only(value)
                let results = [];
                request.onsuccess = () => {
                    let cursor = request.result;
                    if (cursor) {
                        results.push(cursor.value);
                        cursor.continue();
                    } else {
                        observer.next(results);
                        db.close();
                        observer.complete();
                    }
                };
                db.onerror = (e: any) => {
                    db.close();
                    self.handleError("IndexedDB error: " + e.target.errorCode);
                }
            });
        });
    };

    all(source: string, filter?: any): Observable<any[]> {
        let self = this;

        return Observable.create((observer: any) => {
            let indexName = 'id_idx';

            this.open().subscribe((db: any) => {
                let tx = db.transaction(source, "readonly");
                let store = tx.objectStore(source);
                let index = store.index(indexName);
                let request = index.openCursor(); //IDBKeyRange.only("Fred")
                let results: any[] = [];

                request.onsuccess = () => {
                    let cursor = request.result;
                    if (cursor) {
                        results.push(cursor.value);
                        cursor.continue();
                    } else {
                        observer.next(results);
                        db.close();
                        observer.complete();
                    }
                };
                db.onerror = (e: any) => {
                    db.close();
                    self.handleError("IndexedDB error: " + e.target.errorCode);
                }
            });
        });
    };

    count(source: string): Observable<number> {
        let self = this;

        return Observable.create((observer: any) => {
            this.open().subscribe((db: any) => {
                let indexName = 'id_idx';
                let tx = db.transaction(source, "readonly");
                let store = tx.objectStore(source);
                let index = store.index(indexName);
                let request = index.count();

                request.onsuccess = () => {
                    observer.next(request.result);
                    db.close();
                    observer.complete();
                };
                db.onerror = (e: any) => {
                    db.close();
                    self.handleError("IndexedDB error: " + e.target.errorCode);
                }
            });
        });
    };

    private handleError(msg: string) {
        console.error(msg);
        return Observable.throw(msg);
    }

    private open(): Observable<any> {
        let self = this;

        return Observable.create((observer: any) => {
            let request = self.indexedDB.open(self.dbName);

            request.onsuccess = () => {
                observer.next(request.result);
                observer.complete();
            }
            request.onerror = () => {
                self.handleError(request.error);
            }
        });
    }
}
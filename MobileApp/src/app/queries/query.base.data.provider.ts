﻿import { Observable } from 'rxjs/Observable';
import { QueryDataStrategy } from '../infrastructure/query.data.strategy';
import { QueryDataProvider } from '../infrastructure/query.data.provider';

export abstract class QueryBaseDataProvider implements QueryDataProvider {
    private strategy: QueryDataStrategy;

    setStrategy(strategy: QueryDataStrategy): void {
        this.strategy = strategy;
    }

    getById(source: string, id: number): Observable<any> {
        return this.strategy.get(source, id);
    };

    get(source: string, filter: any): Observable<any[]> {
        return this.strategy.get(source, filter);
    };

    all(source: string, filter?: any): Observable<any[]> {
        return this.strategy.all(source);
    };

    count(source: string): Observable<any> {
        return this.strategy.count(source);
    };
}
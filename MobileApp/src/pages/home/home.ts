import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { TranslatePipe, TranslateService } from "ng2-translate";
import { GenericComponent } from './generic.component';

@Component({
    templateUrl: 'home.html'
})
export class HomePage {

    constructor(private navCtrl: NavController) {
    }
}
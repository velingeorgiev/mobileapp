import { Component, Input } from '@angular/core';
import { TranslatePipe, TranslateService } from 'ng2-translate';
import { PostCommand } from '../../app/commands/post.command';

@Component({
    selector: "generic-selector",
    templateUrl: 'generic.component.html'
})
export class GenericComponent {
    private title: string;  
    @Input() heading: string; 
     
    constructor() {} 

    ngOnInit() {
        this.title = this.heading; 
    }
}

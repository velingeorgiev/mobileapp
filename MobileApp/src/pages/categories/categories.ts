import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';

import { Page } from '../../app/infrastructure/page';
import { LoggerProvider } from '../../app/infrastructure/logger.provider';
import { GenericQuery } from '../../app/queries/generic.query';
import { Category, CategoryType, CategoriesMode } from '../../app/domains/category';
import { Suggestion } from '../../app/domains/suggestion';


import { ChallengesPage } from '../challenges/challenges';
import { ChallengePage } from '../challenges/challenge.page';
import { Challenge } from '../../app/domains/challenge';
import { PostChallengeCommand } from '../../app/commands/post.challenge.command';
import { PutCommand } from '../../app/commands/put.command';
import { AchievementEvent } from '../../app/events/achievement.event';
import { SuggestionEvent } from '../../app/events/suggestion.event';
import { AlbumPage } from '../albums/album';


@Component({
    templateUrl: 'categories.html'
})
export class CategoriesPage implements Page {
    private list: Category[];
    private categoriesMode: CategoriesMode;

    constructor(
        private nav: NavController,
        private navParams: NavParams,
        private query: GenericQuery,
        private postChallenge: PostChallengeCommand,
        private putCommand: PutCommand,
        private logger: LoggerProvider,
        private achievementEvent: AchievementEvent,
        private suggestionEvent: SuggestionEvent
    ) { 
        this.categoriesMode = CategoriesMode.Normal;
    }

    ngOnInit(): void {
        this.load();

        this.suggestionEvent.onUpdated.subscribe(resultSuggestion => {
            this.setCategoryBadges();
        });
    }

    onSubmit(category: Category): void {

        this.logger.info(`CategoriesPage.onSubmit|category: ${JSON.stringify(category)}, categoriesMode: ${this.categoriesMode}.`);

        switch (this.categoriesMode) {
            case CategoriesMode.Normal: // 0
                this.challenge(category);
                break;
            case CategoriesMode.Album: // 1
                this.nav.push(AlbumPage, { category: category });
                break;
            case CategoriesMode.HideChallenges: // 3
                this.nav.push(ChallengesPage, { category: category, categoriesMode: this.categoriesMode });
                break;
        }
    }

    private load(): void {
        this.categoriesMode = CategoriesMode.Normal;
        if (this.navParams.data.categoriesMode) {
            this.categoriesMode = this.navParams.data.categoriesMode as CategoriesMode;
        }
        this.logger.info(`CategoriesPage.load|mode: ${this.categoriesMode}.`);

        this.query.setSource("categories");
        this.query.all().subscribe((all: Category[]) => {

            this.list = this.getCategories(all, this.categoriesMode);
            this.setCategoryBadges();
        });
    }

    private challenge(category: Category): void {

        if (category.type === CategoryType.Limited) {

            this.logger.info(`CategoriesPage.onSubmit|ChallengesPage redirect|category: ${JSON.stringify(category)}.`);

            this.nav.push(ChallengesPage, { category: category, categoriesMode: this.categoriesMode });

        } else {

            let challenge = Challenge.create(category.name, category.id);

            this.postChallenge.execute(category.name, challenge).subscribe((resultChallenge: Challenge) => {

                // Update category and challenge based achievements.
                this.achievementEvent.emitIncrementCategory(category);
                this.achievementEvent.emitIncrementChallenge(challenge);

                this.logger.info(`CategoriesPage.onSubmit|ChallengePage redirect|resultChallenge: ${JSON.stringify(resultChallenge)}.`);

                this.nav.push(ChallengePage, { challenge: resultChallenge });

            });
        }
    }

    private toggleCategory(event: any, category: Category): void {

        this.logger.info(`CategoriesPage.toggleCategory|category: ${JSON.stringify(category)}.`);

        this.putCommand.execute("categories", category).subscribe((resultCategory: Category) => {
            this.logger.info(`CategoriesPage.toggleCategory|success|resultChallenge: ${JSON.stringify(resultCategory)}.`);
        });
    }

    private getCategories(categories: Category[], categoryMode: CategoriesMode): Category[] {
        
        if (categoryMode !== CategoriesMode.HideCategories) 
            categories = categories.filter(x => x.active === true) as Category[];

        return categories;
    }

    private setCategoryBadges(): void {

        this.query.setSource("suggestions");

        for (let i = 0; i < this.list.length; i++) {
            
            let category = this.list[i];
            if (category.type !== CategoryType.Limited || category.active !== true)
                continue;

            this.query.get({ index: "categoryId_idx", value: category.id })
                .subscribe((suggestions: Suggestion[]) => {

                    let activeSuggestions = suggestions.filter(x => x.active === true);

                    category.total = activeSuggestions.length;
                    category.completed = activeSuggestions.filter(x => x.completed === true).length;

                    this.logger.info(`Category ${category.name} with total of: ${category.total}.`);
                    this.logger.info(`Category ${category.name} with completed of: ${category.completed}.`);
            });
        }
    }
}


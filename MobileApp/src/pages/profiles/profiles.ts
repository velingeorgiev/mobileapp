import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';

import { Page } from '../../app/infrastructure/page';
import { ProfilePage } from './profile';
import { GenericQuery } from '../../app/queries/generic.query';
import { DataList } from '../../app/infrastructure/data.list';
import { ProfileEvent } from '../../app/events/profile.event';

@Component({
    templateUrl: 'profiles.html'
})
export class ProfilesPage implements Page {
    private list: any[];

    constructor(
        private navCtrl: NavController,
        private event: ProfileEvent,
        private dataList: DataList,
        private query: GenericQuery
    ) { }

    ngOnInit() {

        // Set the data source for the generic query.
        this.query.setSource('profiles');

        // Get the latest data.
        this.query.all().subscribe(all => this.list = all.filter(x => x.isActive === true));

        // Subscribe for profile events and change the list offline if such event is raisen.
        this.event.onAdded.subscribe(profile => this.dataList.add(this.list, profile, profile.id));
        this.event.onUpdated.subscribe(profile => this.dataList.update(this.list, profile));
        this.event.onDeleted.subscribe(profile => this.dataList.remove(this.list, profile));
    }

    onSubmit(profile: any): void {
        this.navCtrl.push(ProfilePage, profile);
    }

    onAdd(): void {
        this.navCtrl.push(ProfilePage, {});
    }
}

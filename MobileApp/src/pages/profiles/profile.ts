import { Component } from '@angular/core';
import { NavParams, NavController, AlertController  } from 'ionic-angular';
import { TranslateService } from "ng2-translate/ng2-translate";
import { Validators, FormBuilder } from '@angular/forms';

import { Form } from '../../app/infrastructure/form';
import { Context } from '../../app/infrastructure/context';
import { Profile } from '../../app/domains/profile';
import { PutCommand } from '../../app/commands/put.command';
import { PostCommand } from '../../app/commands/post.command';
import { ProfileEvent } from "../../app/events/profile.event";
import { DateProvider } from '../../app/infrastructure/date.provider';

import { ToastController } from 'ionic-angular';

@Component({
    templateUrl: 'profile.html'
})
export class ProfilePage extends Form {
    private profile: Profile;

    constructor(
        private nav: NavController,
        private navParams: NavParams,
        private event: ProfileEvent,
        private put: PutCommand,
        private post: PostCommand,
        private toast: ToastController,
        private translate: TranslateService,
        public alertCtrl: AlertController,
        public context: Context,
        fb: FormBuilder
    ) {
        super(fb);
    }

    protected init() { this.profile = this.navParams.data; }

    protected setFormSchema(): any {
        return {
            name: [this.profile.name, Validators.required]
        };
    }

    protected setFormErrors(): any {
        return {
            "name": ""
        };
    }

    protected setValidationMessages(): any {
        return {
            "name": {
                "required": "Required"
            }
        };
    }

    onSubmit(): void {
        let self = this;
        // Re-map from the form controls.

        this.profile.name = this.form.value.name;  // TODO: see if better way to do it. Factory?

        if (this.profile.id) {

            self.profile.modified = DateProvider.now();

            this.put.execute('profiles', this.profile).subscribe(done => {
                self.event.emitUpdated(self.profile);
                self.nav.pop();
            });


        } else {

            self.profile.isActive = true;

            this.post.execute('profiles', this.profile).subscribe(id => {

                self.profile.id = id;
                self.profile.created = DateProvider.now();
                self.profile.modified = DateProvider.now();

                self.event.emitAdded(self.profile);
                this.showSuccessMessage();
            });
        }
    }

    onDelete(): void {

        let activeProfile = this.context.getActiveProfile();
        if (activeProfile.id == this.profile.id) {
            this.showCannotDeleteAlert();
        } else {
            this.showDeleteConfirm();
        }
    }

    private delete(): void {
        this.profile.isActive = false;

        this.put.execute("profiles", this.profile)
            .subscribe(success => {

                this.event.emitDeleted(this.profile);
                this.showSuccessMessage();
                this.nav.pop();

        });
    }

    private showDeleteConfirm(): void {
        this.translate.get("Delete_profile_title")
            .subscribe((title: string) => {
                this.translate.get("Delete_profile_text")
                    .subscribe((text: string) => {
                        this.translate.get("Cancel")
                            .subscribe((cancelText: string) => {
                                this.translate.get("Delete")
                                    .subscribe((deleteText: string) => {

                                        let confirm = this.alertCtrl.create({
                                            title: title,
                                            message: `${text}${this.profile.name}`,
                                            buttons: [
                                                {
                                                    text: cancelText,
                                                    handler: () => { }
                                                },
                                                {
                                                    text: deleteText,
                                                    handler: () => { this.delete(); }
                                                }
                                            ]
                                        });
                                        confirm.present();

                                    });
                            });
                    });
            });
    }

    private showCannotDeleteAlert(): void {
        this.translate.get("Cannot_delete_profile_title")
            .subscribe((title: string) => {
                this.translate.get("Cannot_delete_profile_text")
                    .subscribe((text: string) => {

                        let alert = this.alertCtrl.create({
                            title: title,
                            subTitle: text,
                            buttons: ['OK']
                        });
                        alert.present();
                    });
            });
    }

    private showSuccessMessage(): void {

        this.translate.get("Success").subscribe((res: string) => {
            let toast = this.toast.create({
                message: res,
                duration: 3000
            });
            toast.present();
        });
    }
}
// https://ionicframework.com/docs/v2/resources/forms/
// http://www.gajotres.net/ionic-2-how-o-create-and-validate-forms/2/

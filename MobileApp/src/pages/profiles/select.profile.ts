import { Component, Input } from '@angular/core';
import { NavController } from 'ionic-angular';

import { ProfilesPage } from './profiles';
import { Context } from '../../app/infrastructure/context';
import { ProfileEvent } from '../../app/events/profile.event';

@Component({
    selector: "select-profile",
    template: "<button type=\"button\" ion-button (click)=\"onSelect()\">{{ 'Activate' | translate }}</button>"
})
export class SelectProfile {
    @Input() profile: any; 

    constructor(
        private nav: NavController,
        private context: Context,
        private profileEvent: ProfileEvent
    ) {}

    onSelect(): void {

        this.context.setObject('profile', this.profile);
        this.profileEvent.emitUpdated(this.profile);

        this.nav.pop();
    }
}

import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';

import { Page } from '../../app/infrastructure/page';
import { LoggerProvider } from '../../app/infrastructure/logger.provider';
import { Achievement } from '../../app/domains/achievement';
import { Category, CategoryType, CategoriesMode } from '../../app/domains/category';
import { PutCommand } from '../../app/commands/put.command';
import { GenericQuery } from '../../app/queries/generic.query';
import { AchievementPage } from './achievement';

@Component({
    templateUrl: 'achievements.html'
})
export class AchievementsPage implements Page {
    private achievements: Achievement[];

    constructor(
       private nav: NavController,
       private navParams: NavParams,
       private logger: LoggerProvider,
       private query: GenericQuery
    ) {}

    ngOnInit(): void {
        this.achievements = [];
        this.query.setSource("achievements");
        this.query.all().subscribe(all => all.forEach(x => this.achievements.push(Achievement.create(x))));

        this.logger.info(`AchievementsPage.ngOnInit|achievements:${JSON.stringify(this.achievements)}.`);
    }

    onSubmit(achievement: Achievement): void {

        this.logger.info(`AchievementsPage.onSubmit|achievement: ${JSON.stringify(achievement)}.`);
        this.nav.push(AchievementPage, { achievement: achievement });

    }
}

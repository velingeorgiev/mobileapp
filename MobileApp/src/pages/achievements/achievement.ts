import { Component, OnInit  } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';

import { Achievement } from '../../app/domains/achievement';
import { LoggerProvider } from '../../app/infrastructure/logger.provider';

@Component({
    templateUrl: 'achievement.html'
})
export class AchievementPage implements OnInit {
    private achievement: Achievement;

    constructor(
        private nav: NavController,
        private navParams: NavParams,
        private logger: LoggerProvider
    ) { }

    ngOnInit(): void {

        this.achievement = this.navParams.data.achievement as Achievement;
        this.logger.info(`AchievementPage.ngOnInit|achievement:${JSON.stringify(this.achievement)}.`);
    }
}

import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';

@Component({
    templateUrl: 'skins.html'
})
export class SkinsPage {
  constructor(public navCtrl: NavController) {
  }
}

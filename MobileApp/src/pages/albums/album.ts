import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';

import { Page } from '../../app/infrastructure/page';
import { Context } from '../../app/infrastructure/context';
import { GenericQuery } from '../../app/queries/generic.query';
import { Challenge } from '../../app/domains/challenge';
import { ChallengePage } from '../challenges/challenge.page';
import { Profile } from '../../app/domains/profile';
import { Category, CategoryType } from '../../app/domains/category';
import { ChallengeEvent } from '../../app/events/challenge.event';
import { DataList } from '../../app/infrastructure/data.list';

@Component({
    templateUrl: 'album.html'
})
export class AlbumPage implements Page {
    private list: Challenge[];
    private category: Category;
    private profile: Profile;

    constructor(
        private nav: NavController,
        private navParams: NavParams,
        private query: GenericQuery,
        private context: Context,
        private challengeEvent: ChallengeEvent,
        private dataList: DataList
    ) {
        this.category = navParams.data.category as Category;
        this.profile = context.getActiveProfile() as Profile;
    }

    ngOnInit(): void {
        this.query.setSource("challenges");
        let filter = { index: "categoryId_idx", value: this.category.id };
        this.query.get(filter)
            .subscribe((challenges: Challenge[]) => {

                this.list = challenges.filter((x: Challenge) => {
                    return x.profileId === this.profile.id;
                });
            });

        // Subscribe for profile events and change the list offline if such event is raisen.
        this.challengeEvent.onAdded.subscribe(challenge => this.dataList.add(this.list, challenge, challenge.id));
        this.challengeEvent.onUpdated.subscribe(challenge => this.dataList.update(this.list, challenge));
        this.challengeEvent.onDeleted.subscribe(challenge => this.dataList.remove(this.list, challenge));
    }

    onSubmit(challenge: Challenge): void {
        this.nav.push(ChallengePage, { challenge: challenge });
    }
}

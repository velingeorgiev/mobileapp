import { Component, OnInit } from '@angular/core';
import { NavController } from 'ionic-angular';

import { Context } from '../../app/infrastructure/context';
import { LoggerProvider } from '../../app/infrastructure/logger.provider';

import { CategoriesPage } from '../categories/categories';
import { CategoriesMode } from '../../app/domains/category';

@Component({
    templateUrl: 'settings.html'
})
export class SettingsPage {
    private hasForcedFacebookUpload: boolean;

    constructor(
        private nav: NavController,
        private context: Context,
        private logger: LoggerProvider
    ) {
        this.hasForcedFacebookUpload = false;
    }

    ngOnInit(): void {
        this.hasForcedFacebookUpload = this.context.get("hasForcedFacebookUpload") as boolean;
        this.logger.info(`SettingsPage.ngOnInit|forceFacebook: ${this.hasForcedFacebookUpload}.`);
    }

    private forceFacebook(): void {
        this.logger.info(`SettingsPage|forceFacebook|this.hasForcedFacebookUpload: ${this.hasForcedFacebookUpload}.`);
        this.context.set("hasForcedFacebookUpload", this.hasForcedFacebookUpload);
    }

    private hideCategories(): void {

        this.logger.info("SettingsPage|hideCategories.");
        this.nav.push(CategoriesPage, { categoriesMode: CategoriesMode.HideCategories });
    }

    private hideChallenges(): void {

        this.logger.info("SettingsPage|hideChallenges.");
        this.nav.push(CategoriesPage, { categoriesMode: CategoriesMode.HideChallenges });
    }
}

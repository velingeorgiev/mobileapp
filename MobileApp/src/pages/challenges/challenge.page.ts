import { Component, ViewChild } from '@angular/core';
import { NavController, NavParams, Slides, AlertController } from 'ionic-angular';

import { Page } from '../../app/infrastructure/page';
import { Challenge } from '../../app/domains/challenge';
import { LoggerProvider } from '../../app/infrastructure/logger.provider';
import { TranslateService } from 'ng2-translate/ng2-translate';

@Component({
    templateUrl: 'challenge.page.html',
    styleUrls: ['assets/challenge.css']
})
export class ChallengePage implements Page {
    private challenge: Challenge;
    private flip: boolean;
    private currentSlide: string;
    @ViewChild('slides') slider: Slides;

    constructor(
        private nav: NavController,
        private navParams: NavParams,
        private translate: TranslateService,
        private logger: LoggerProvider,
        private alertCtrl: AlertController
    ) {
        this.logger.info(`ChallengePage.init|navParams.data: ${JSON.stringify(this.navParams.data)}.`);

        this.challenge = this.navParams.data.challenge as Challenge;
        // UI view bug hack / fix on some devices.
        setTimeout(x => { this.pageOpacityFix() }, 500);
    }

    ngOnInit(): void {
        this.logger.info(`ChallengePage.init|navParams.data: ${JSON.stringify(this.navParams.data)}.`);
    }

    onFormSubmited(challenge: Challenge): void {
        let self = this;
    }

    onDelete(): void {
        this.showDeleteConfirm();
    }

    onSubmit(): void {

    }

    private pageOpacityFix(): void {
        // Check the alignment on a number of cells in a table. 
        var cells = document.getElementsByTagName("ng-component");
        for (var i = 0; i < cells.length; i++) {
            this.logger.info(`before class name: ${cells[i].className}.`);
            cells[i].className = "ion-page show-page";
            this.logger.info(`after class name: ${cells[i].className}.`);
        }
    }

    private onFlip(): void {
        switch (this.currentSlide) {    
            case "back":
                this.flip = false;
                this.currentSlide = "picture";
                break;
            default:
                this.flip = true;
                this.slider.slideTo(0, 300);
                this.currentSlide = "back";
        }
    }

    //private slideLeft(): void {

    //    switch (this.currentSlide) {
    //        case "back":
    //            this.flip = false;
    //            this.currentSlide = "picture";
    //            break;
    //        case "form":
    //            this.slider.slideTo(0, 200);
    //            this.currentSlide = "back";
    //            break;
    //    }
    //}

    //private slideRight(): void {

    //    switch (this.currentSlide) {
    //        case "picture":
    //            this.slider.slideTo(0, 300);
    //            this.flip = true;
    //            this.currentSlide = "back";
    //            break;
    //        case "back":
    //            this.slider.slideTo(1, 200);
    //            this.currentSlide = "form";
    //            break;
    //    }
    //}

    private onSlideChanged(): void {
        let currentIndex = this.slider.getActiveIndex();
        switch (currentIndex) {
            case 0:
                this.currentSlide = "back";
                break;
            case 1:
                this.currentSlide = "form";
                break;
        }
    }

    private showDeleteConfirm(): void {
        this.translate.get("Delete_challenge_title")
            .subscribe((title: string) => {
                this.translate.get("Delete_challenge_text")
                    .subscribe((text: string) => {
                        this.translate.get("Cancel")
                            .subscribe((cancelText: string) => {
                                this.translate.get("Delete")
                                    .subscribe((deleteText: string) => {

                                        let confirm = this.alertCtrl.create({
                                            title: title,
                                            message: `${text}`,
                                            buttons: [
                                                {
                                                    text: cancelText,
                                                    handler: () => { }
                                                },
                                                {
                                                    text: deleteText,
                                                    handler: () => { this.delete(); }
                                                }
                                            ]
                                        });
                                        confirm.present();
                                    });
                            });
                    });
            });
    }

    private delete(): void {
        // TODO: Add logging.

        //this.removeCommand.execute("challenges", this.challenge.id).subscribe(result => {
        //    let thumb = Picture.create(this.challenge.thumbUrl);

        //    // Delete the thumb.
        //    this.pictureDelete.execute("ChallengePage", thumb).subscribe(result => { });

        //    // Delete the picture.
        //    let picture = Picture.create(this.challenge.pictureUrl);

        //    this.pictureDelete.execute("ChallengePage", picture).subscribe(result => {

        //        this.challengeEvent.emitDeleted(this.challenge);
        //        this.updateSuggestions();
        //        this.nav.pop();
        //        this.showSuccessMessage();
        //    });
        //});
    }

    //private updateSuggestions(): void {
    //    // TODO: Move as separate command.
    //    // TODO: Add logging.

    //    this.query.setSource("challenges");
    //    this.query.get({ index: "name_idx", value: this.challenge.name })
    //        .subscribe(result => {

    //            this.logger.info(`ChallengePage.updateSuggestions|query.get|challenges|result: ${JSON.stringify(result)}.`);

    //            if (!result.length || result.length === 0) {

    //                // No similar challenges so remove the suggestion checked mark.
    //                this.query.setSource("suggestions");

    //                this.query.get({ index: "name_idx", value: this.challenge.name })
    //                    .subscribe((resultSuggestion: Suggestion[]) => {

    //                        this.logger.info(`ChallengePage.updateSuggestions|query.get|suggestions|resultSuggestion: ${JSON.stringify(resultSuggestion[0])}.`);


    //                        resultSuggestion[0].completed = false;

    //                        this.logger.info(`ChallengePage.updateSuggestions|query.get|suggestions|resultSuggestion|updated: ${JSON.stringify(resultSuggestion[0])}.`);

    //                        this.putCommand.execute("suggestions", resultSuggestion[0])
    //                            .subscribe((resultSuggestion2: Suggestion) => {

    //                                this.logger.info(`ChallengePage.updateSuggestions|putCommand|suggestions|resultSuggestion2: ${JSON.stringify(resultSuggestion2)}.`);

    //                                this.suggestionEvent.emitUpdated(resultSuggestion2);
    //                            });
    //                    });
    //            }
    //        });
    //}

    //private showSuccessMessage(): void {

    //    this.translate.get("Success").subscribe((res: string) => {
    //        let toast = this.toast.create({
    //            message: res,
    //            duration: 3000
    //        });
    //        toast.present();
    //    });
    //}
}

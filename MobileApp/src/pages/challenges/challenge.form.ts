import { Component, Input, Output, EventEmitter  } from '@angular/core';
import { Validators, FormBuilder } from '@angular/forms';

import { Form } from '../../app/infrastructure/form';
import { Challenge } from '../../app/domains/challenge';
import { PutCommand } from '../../app/commands/put.command';
import { Context } from '../../app/infrastructure/context';
import { LoggerProvider } from '../../app/infrastructure/logger.provider';
import { FacebookUploadCommand } from '../../app/commands/facebook.upload.command';

@Component({
    selector: "challenge-form",
    templateUrl: "challenge.form.html"
})
export class ChallengeForm extends Form {
    @Input() challenge: Challenge;
    @Output() onFormSubmitted: EventEmitter<Challenge>;
    private isFacebookUploadEnabled: boolean;

    constructor(
        private logger: LoggerProvider,
        private context: Context,
        private putCommand: PutCommand,
        private facebookUploadCommand: FacebookUploadCommand,
        fb: FormBuilder
    ) {
        super(fb);
        this.onFormSubmitted = new EventEmitter<Challenge>();
        this.isFacebookUploadEnabled = false;
    }

    init(): void {
        this.isFacebookUploadEnabled = (this.context.get("isFacebookUploadEnabled") == true);
    }

    protected setFormSchema(): any {
        return {
            memory: [this.challenge.memory],
            isFacebookUploadEnabled: [this.isFacebookUploadEnabled]
        };
    }

    protected setFormErrors(): any {
        return {
            "memory": "",
            "facebookShare": ""
        };
    }

    protected setValidationMessages(): any {
        return {
            "memory": {
                "required": "Required"
            },
            "facebookShare": {
                "required": "Required"
            }
        };
    }

    onSubmit(): void {
        let self = this;

        this.logger.info(`ChallengeForm.onSubmit|this.form.value: ${JSON.stringify(this.form.value)}.`);

        this.updateMemory();

        this.putCommand.execute("challenges", this.challenge)
            .subscribe(resultChallenge => {

                // Upload to facebook if checked.
                if (this.form.value.isFacebookUploadEnabled)
                    this.facebookUploadCommand.execute("ChallengeForm", resultChallenge).subscribe(result => { });

                // Talk to the parrent.
                this.onFormSubmitted.emit(resultChallenge);
            });
    }

    private updateMemory(): void {

        let memory = this.form.value.memory;
        if (memory !== undefined && memory.length > 0) {
            this.challenge.memory = memory;
        } else {
            this.challenge.memory = "";
        }
    }
}

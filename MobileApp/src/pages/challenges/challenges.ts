import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';

import { Page } from '../../app/infrastructure/page';
import { LoggerProvider } from '../../app/infrastructure/logger.provider';
import { Challenge } from '../../app/domains/challenge';
import { Category, CategoryType, CategoriesMode } from '../../app/domains/category';
import { Suggestion } from '../../app/domains/suggestion';

import { PutCommand } from '../../app/commands/put.command';
import { GenericQuery } from '../../app/queries/generic.query';
import { PostChallengeCommand } from '../../app/commands/post.challenge.command';
import { ChallengePage } from '../challenges/challenge.page';
import { HomePage } from '../home/home';
import { AchievementEvent } from '../../app/events/achievement.event';
import { SuggestionEvent } from '../../app/events/suggestion.event';

@Component({
    templateUrl: 'challenges.html'
})
export class ChallengesPage implements Page {
    private category: Category;
    private categoriesMode: CategoriesMode;
    private suggestions: Suggestion[];

    constructor(
        private nav: NavController,
        private navParams: NavParams,
        private logger: LoggerProvider,
        private postChallenge: PostChallengeCommand,
        private putCommand: PutCommand,
        private query: GenericQuery,
        private achievementEvent: AchievementEvent,
        private suggestionEvent: SuggestionEvent
    ) { }

    ngOnInit(): void {
        this.category = this.navParams.data.category as Category;
        this.categoriesMode = this.navParams.data.categoriesMode as CategoriesMode;

        this.getSuggestions(this.category, this.categoriesMode);

        this.logger.info(`ChallengesPage.ngOnInit|category: ${JSON.stringify(this.category)}, mode: ${this.categoriesMode}.`);
    }

    onSubmit(suggestion: Suggestion): void {
        let self = this;

        this.logger.info(`ChallengesPage.onSubmit|suggestion: ${JSON.stringify(suggestion)}, categoriesMode: ${this.categoriesMode}.`);

        let challenge = Challenge.create(suggestion.name, this.category.id);

        this.postChallenge
            .execute("challenges", challenge)
            .subscribe((resultChallenge: Challenge) => {

                self.logger.info(`ChallengesPage.onSubmit|ChallengePage redirect|resultChallenge: ${JSON.stringify(resultChallenge)}.`);

                // Update challenge based achievements.
                self.achievementEvent.emitIncrementChallenge(resultChallenge);

                self.nav.push(ChallengePage, { challenge: challenge });  

                if (suggestion.completed !== true)
                    self.updateSuggestion(suggestion);
            });
    }

    private toggleSuggestion(event: any, suggestion: Suggestion): void {

        this.putCommand
            .execute("suggestions", suggestion)
            .subscribe((resultSuggestion: Suggestion) => {
                this.logger.info(`ChallengesPage.toggleSuggestion|success|resultSuggestion: ${JSON.stringify(resultSuggestion)}.`);
            });
    }

    private getSuggestions(category: Category, categoriesMode: CategoriesMode): void {

        this.query.setSource("suggestions");
        this.query.get({ index: "categoryId_idx", value: category.id }).subscribe((all: Suggestion[]) => {

            this.suggestions = all;

            if (categoriesMode !== CategoriesMode.HideChallenges)
                this.suggestions = this.suggestions.filter(x => x.active === true);

            this.logger.info(`ChallengesPage.getSuggestions|suggestions: ${JSON.stringify(this.suggestions)}.`);
        });
    }

    private updateSuggestion(suggestion: Suggestion): void {

        // suggestion.setCompleted();
        suggestion.completed = true;

        this.putCommand
            .execute("suggestions", suggestion)
            .subscribe((resultSuggestion: Suggestion) => {

                // Update suggestion based achievements.
                this.achievementEvent.emitIncrementSuggestion(resultSuggestion);

                // Raise suggestion event.
                this.suggestionEvent.emitUpdated(resultSuggestion);

                this.logger.info(`ChallengesPage.updateSuggestion|success|resultSuggestion: ${JSON.stringify(resultSuggestion)}.`);
            });
    }
}
